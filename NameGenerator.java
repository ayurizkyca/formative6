import java.util.Random;
import java.util.Scanner;

public class NameGenerator {

    public static void main(String[] args) {
        String nama;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan Nama untuk di generate : ");
        nama = keyboard.nextLine();
        System.out.println("Hasil Generate Nama : " + generateNama(nama));
        keyboard.close();
    }

    private static String generateNama(String huruf){
        // String huruf = "Ayu Rizkyca Awalia";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 1; i <= huruf.length(); i++) {
            int index = random.nextInt(huruf.length());
            char randomChar = huruf.charAt(index);
            sb.append(randomChar);
        }
        String randomString = sb.toString();
        return randomString;
    }
}