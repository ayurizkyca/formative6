import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class Task3 {
    public static void main(String[] args) {
        String[] names = { "Ayu Rizkyca Awalia", "Viviana", "Rizky Wahyudi", "Virgilius",
                "Ammar Muadz", "Ammar Fathin", "Iqbal Rahman", "Ansell", "Diar Ihza", "Hafid Nugroho" };

        System.out.println("=====Shuffled Name Characters======");
        for (String name : names) {
            System.out.println("Real Name : " + name);
            String shuffledName = shuffleName(name);
            System.out.println("Shuffled name : " + shuffledName + "\n");
        }
        System.out.println("Check the real name (input shuffled name) :");
        Scanner keyboard = new Scanner(System.in);
        String restoreName = keyboard.nextLine();
        System.out.println("Restore name : " + findName(restoreName, names));

        keyboard.close();
    }

    private static String shuffleName(String name) {
        char[] characters = name.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            int j = new Random().nextInt(characters.length);
            char temp = characters[i];
            characters[i] = characters[j];
            characters[j] = temp;
        }
        return new String(characters);
    }

    private static Set<Character> createCharSet(String str) {
        Set<Character> charSet = new HashSet<>();
        for (char c : str.toCharArray()) {
            charSet.add(c);
        }
        return charSet;
    }

    private static boolean isSimilar(String shuffledName, String realName) {
        Set<Character> charSet1 = createCharSet(shuffledName);
        Set<Character> charSet2 = createCharSet(realName);

        return charSet1.equals(charSet2);
    }

    private static String findName(String shuffledName, String[] realNames) {
        for (String originalName : realNames) {
            if (isSimilar(shuffledName, originalName)) {
                return originalName;
            }
        }
        return "there's no name on the list";
    }
}
