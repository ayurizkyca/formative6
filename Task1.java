import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class Task1 {

    public static void main(String[] args) {
        // Membuat thread untuk Jakarta
        Thread jakartaThread = new Thread(() -> platAmountAndSaveToFile(20, "B", "Jakarta.txt"));

        // Membuat thread untuk Bogor
        Thread bogorThread = new Thread(() -> platAmountAndSaveToFile(10, "F", "Bogor.txt"));

        // Membuat thread untuk Tangerang
        Thread tangerangThread = new Thread(() -> platAmountAndSaveToFile(0, "A", "Tangerang.txt"));

        // Menjalankan thread
        jakartaThread.start();
        bogorThread.start();
        tangerangThread.start();
    }

    //generate plat number
    public static String generatePlat(String kodeKota){
        StringBuilder sb = new StringBuilder();
        return sb.append(kodeKota).append(" ").append(generateAngka())
        .append(" ").append(generateHuruf()).toString();
    }

    public static String generateAngka(){
        String angka = "1234";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 1; i <= angka.length(); i++) {
            int index = random.nextInt(angka.length());
            char randomChar = angka.charAt(index);
            sb.append(randomChar);
        }
        String randomString = sb.toString();
        return randomString;
    }

    public static String generateHuruf(){
        String angka = "ABC";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 1; i <= 2; i++) {
            int index = random.nextInt(angka.length());
            char randomChar = angka.charAt(index);
            sb.append(randomChar);
        }
        String randomString = sb.toString();
        return randomString;
    }

    //save output to file
    public static void saveOutputToFile(Object output, String fileName) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(fileName))) {
            // Menulis output ke dalam file
            writer.println(output);
            System.out.println("Succes save output to file: " + fileName);
        } catch (IOException e) {
            // Tangkap pengecualian jika terjadi kesalahan
            System.err.println("Save output to file is failed: " + e.getMessage());
        }
    }

    public static void platAmountAndSaveToFile(int num, String regionCode, String fileName) {
        StringBuilder sb = new StringBuilder();

        for (int i = 1; i <= num; i++) {
            String platNumber = generatePlat(regionCode);
            sb.append(i).append(". ").append(platNumber).append("\n");
        }

        saveOutputToFile(sb.toString(), fileName);
    }
}