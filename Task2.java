import java.util.Random;

public class Task2 {
    public static void main(String[] args) {
        String[] names = {"Ayu Rizkyca Awalia", "Viviana", "Rizky Wahyudi", "Virgilius", 
        "Ammar Muadz", "Ammar Fathin", "Iqbal Rahman", "Ansell", "Diar Ihza", "Hafid Nugroho"};

        System.out.println("Shuffled Name Characters");
        for (String name : names){
            System.out.println("Real Name : " + name);
            System.out.println("Shuffled name : " + shuffleName(name) + "\n");
        }
    }

    private static String shuffleName(String name){
        char[] characters = name.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            int j = new Random().nextInt(characters.length);
            char temp = characters[i];
            characters[i] = characters[j];
            characters[j] = temp;
        }
        return new String(characters);
    }
}
